# Frontend Mentor - Interactive Pricing Component

This is a solution to the [Tip calculator app](https://www.frontendmentor.io/challenges/tip-calculator-app-ugJNGbJUX). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Calculate the correct tip and total cost of the bill per person

### Screenshot

![](./screenshot.jpg)

### Links

- Solution URL: [https://bitbucket.org/nmoraja/tip-calculator-app/src](https://bitbucket.org/nmoraja/tip-calculator-app/src)
- Live Site URL: [https://nmorajda-tip-calculator-app.netlify.app/](https://nmorajda-tip-calculator-app.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- JavaScript ES6

## Author

- Website - [N. Morajda](https://abmstudio.pl)
- Frontend Mentor - [@nmorajda](https://www.frontendmentor.io/profile/nmorajda)
- Github - [nmorajda](https://github.com/nmorajda)
- Bitbucket - [nmorajda](https://bitbucket.org/nmoraja/)
